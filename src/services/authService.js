const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');
const {LoginError} = require('../utils/errors');

const register = async ({email, password, role}) => {
    const user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role
    });
    await user.save();
}

const login = async ({email, password}) => {
    const user = await User.findOne({email});
    
    if (!user) {
      throw new LoginError('Invalid username or password');
    }

    if (!(await bcrypt.compare(password, user.password))) {
      throw new LoginError('Invalid username or password');
    }

    const token = jwt.sign({
        userId: user._id,
        email: user.email
    }, 'secret');
    return token;
}

module.exports = {
  register,
  login
};
