const {Load, LOAD_STATES} = require('../models/loadModel');

const getLoadsByUserId = async (userId) => {
    const trucks = await Load.find({created_by: userId})
    return trucks;
}

const addLoadToUser = async (userId, name, payload, pickup_address, delivery_address, width, length, height) => {
    const checkForExist = await Load.findOne({created_by: userId, name: name})
    if(!checkForExist) {
        const load = new Load({created_by: userId, name: name, payload: payload,
                                pickup_address: pickup_address, delivery_address: delivery_address,
                                dimensions: { width, length, height } });
        await load.save();
        return true
    } else {
        return false
    }
}

const getLoadById = async (id) => {
  const load = await Load.findOne({_id: id});
  return load;
}

const getLoadByDriverId = async (driverId) => {
    const load = await Load.findOne({assigned_to: driverId});
    return load;
}



const deleteLoadById = async (id, userId) => {
    const load = await Load.deleteOne({_id: id, created_by: userId});
    return load.deletedCount;
}
  
const putLoadById = async (id, userId, name, payload, pickup_address, delivery_address, {width, length, height}) => {
    const load = await Load.updateOne({_id: id, created_by: userId}, {name: name, payload: payload,
        pickup_address: pickup_address, delivery_address: delivery_address,
        dimensions: { width, length, height }});
    return load;
}


const updateLoadById = async (id, status, assigned_to, state) => {
    const load = await Load.updateOne({_id: id}, {status: status, assigned_to: assigned_to, state});
    return load;
}

const iterateLoadState = async (id, state) => {
    let i = 0;
    let load;
    console.log(state)
    console.log(i)
    for (i = 0; i < LOAD_STATES.length; i++)
    {
        console.log(i)
        if (state === LOAD_STATES[i]) {
            break;
        }
    }
    if (i === 3) return;
    if (i === 2) 
    {
        console.log(i)
        load = await Load.updateOne({_id: id}, {state: LOAD_STATES[i+1], status: 'SHIPPED'})
    }
    else {
        console.log(i)
        load = await Load.updateOne({_id: id}, {state: LOAD_STATES[i+1]})
    }
    return load;
}

module.exports = {
    getLoadsByUserId,
    addLoadToUser,
    getLoadById,
    deleteLoadById,
    putLoadById,
    getLoadByDriverId,
    updateLoadById,
    iterateLoadState
};
