const {Truck} = require('../models/truckModel');
const { TRUCK_STATUSES, TRUCK_TYPES } = require('../models/truckModel');


const getTrucksByUserId = async (userId) => {
    const trucks = await Truck.find({created_by: userId})
    return trucks;
}

const addTruckToUser = async (userId, type) => {
    const checkForExist = await Truck.findOne({created_by: userId, type: type})
    if(!checkForExist) {
        const truck = new Truck({type: type, created_by: userId});
        await truck.save();
        return true
    } else {
        return false
    }
}

const getTruckById = async (id, userId) => {
  const truck = await Truck.findOne({_id: id, created_by: userId});
  return truck;
}

const deleteTruckById = async (id, userId) => {
    const truck = await Truck.deleteOne({_id: id, created_by: userId});
    return truck.deletedCount;
}

const assignTruckToUser = async (id, userId) => {
    const truck = await Truck.updateOne({_id: id, created_by: userId}, {assigned_to: userId});
    return truck;
}


const getAssignedTruckByUserId = async (userId) => {
    const truck = await Truck.findOne({assigned_to: userId});
    return truck;
}
  
const putTruckById = async (id, userId, type) => {
    const truck = await Truck.updateOne({_id: id, created_by: userId}, {type: type});
    return truck;
}

const findTruckToLoad = async (payload,  width, length, height) => {
    let type = null;
    let load = null;
    for (i = 0; i < TRUCK_TYPES.length; i++)
    {
        let truck = TRUCK_TYPES[i];
        if (truck.payload > payload && truck.dimensions.width > width && truck.dimensions.length > length && truck.dimensions.height > height)
        {
            type = TRUCK_TYPES[i].type;
            break;
        }
    }
    if (type) 
    {
        truck = await Truck.findOneAndUpdate({'assigned_to': {$ne: null},  status: TRUCK_STATUSES[0], type},
        {status: 'OL'});
        console.log(truck);
        return truck;
    }    
    return false;
}

module.exports = {
    getTrucksByUserId,
    addTruckToUser,
    getTruckById,
    deleteTruckById,
    assignTruckToUser,
    putTruckById,
    getAssignedTruckByUserId,
    findTruckToLoad
};
