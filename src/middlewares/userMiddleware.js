const { ROLE } = require('../models/userModel');

const {
    RoleError
} = require('../utils/errors');


const driverRole = (req, res, next) => {
  console.log(req.user.role);
    if (req.user.role != ROLE[0]) {
      throw new RoleError('No rights!');
    }
    next();
  };
  
const shipperRole = (req, res, next) => {
    if (req.user.role != ROLE[1]) {
      throw new RoleError('No rights!');
    }
    next();
  };
  
  module.exports = {
    driverRole,
    shipperRole,
  };