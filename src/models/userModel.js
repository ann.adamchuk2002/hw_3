const mongoose = require('mongoose');

const ROLE = {
    shipper: 'SHIPPER',
    driver: 'DRIVER',
  };

const User = mongoose.model('User', {
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { User, ROLE };
