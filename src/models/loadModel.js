const mongoose = require('mongoose');

const LOAD_STATUSES = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];

const LOAD_STATES = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to Delivery',
  'Arrived to Delivery',
];

const dimensionsSchema = new mongoose.Schema({
  width: {
    type: Number,
    required: true,
  },
  length: {
    type: Number,
    required: true,
  },
  height: {
    type: Number,
    required: true,
  },
});

const logSchema = new mongoose.Schema({
  message: {
    type: String,
    default: 'Load created',
  },
  time: {
    type: Date,
    default: Date.now,
  },
});

const Load = mongoose.model('Load', {
    created_by:  {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    assigned_to: {
      type: String,
      default: null,
    },
    status: {
      type: String,
      default: LOAD_STATUSES[0],
    },
    state: {
      type: String,
      default: LOAD_STATES[0],
    },
    name: {
      type: String,
      required: true,
    },
    payload: {
      type: Number,
      required: true,
    },
    pickup_address: {
      type: String,
      required: true,
    },
    delivery_address: {
      type: String,
      required: true,
    },
    dimensions: {
      type: {dimensionsSchema},
      required: true,
    },
    logs: {
      type: [logSchema],
      default: () => ({}),
    },
    created_date: {
      type: Date,
      default: Date.now(),
    },
  });



  module.exports = { Load, LOAD_STATES, LOAD_STATUSES };