const mongoose = require('mongoose');

const TRUCK_STATUSES = ['IS', 'OL'];

const TRUCK_TYPES = [
  {
    type: 'SPRINTER',
    dimensions: {
      width: 300,
      length: 250,
      height: 170,
    },
    payload: 1700,
  },
  {
    type: 'SMALL STRAIGHT',
    dimensions: {
      width: 500,
      length: 250,
      height: 170,
    },
    payload: 2500,
  },
  {
    type: 'LARGE STRAIGHT',
    dimensions: {
      width: 700,
      length: 350,
      height: 200,
    },
    payload: 4000,
  },
];


const Truck = mongoose.model('Truck', {
    created_by:  {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    assigned_to: {
      type: mongoose.Schema.Types.ObjectId,
      default: null,
    },
    type: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      default: TRUCK_STATUSES[0],
    },
    createdDate: {
      type: Date,
      default: Date.now(),
    },
  });

module.exports = { Truck, TRUCK_STATUSES, TRUCK_TYPES };
