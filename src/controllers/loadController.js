const express = require('express');
const router = express.Router();
const {driverRole, shipperRole} = require('../middlewares/userMiddleware'); 

const {
    getLoadsByUserId,
    addLoadToUser,
    getLoadById,
    deleteLoadById,
    putLoadById,
    getLoadByDriverId,
    updateLoadById,
    iterateLoadState
} = require('../services/loadService');

const {
    findTruckToLoad
} = require('../services/truckService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

const {
    InvalidRequestError
} = require('../utils/errors');

router.get('/', shipperRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    console.log("AFDS");
    const loads = await getLoadsByUserId(userId);
    res.json({ 
        "loads": loads
    })
}));

router.get('/active', driverRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const load = await getLoadByDriverId(userId)
    if (!load) {
        throw new InvalidRequestError('No Load with such id found!');
    }
    res.json({load});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const load = await getLoadById(id);
    if (!load) {
        throw new InvalidRequestError('No Load with such id found!');
    }
    res.json({load});
}));

router.post('/', shipperRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const {
        name, payload, pickup_address, delivery_address,
        dimensions: {
          width, length, height,
        },
      } = req.body;    
    await addLoadToUser(userId, name, payload, pickup_address, delivery_address, width, length, height);
    res.json({message: "Truck created successfully"});
}));


router.patch('/active/state', driverRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    let load = await getLoadByDriverId(userId);
    if (!load) {
        throw new InvalidRequestError('No Load with such id found!');
    }
    load = await iterateLoadState(load._id, load.state);
    res.json({message:  `Load state changed to ${load.state}`})
}));

router.delete('/:id', shipperRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    await deleteLoadById(id, userId)

    res.json({message: "Load deleted successfully"});
}));

router.put('/:id', shipperRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const {
        name, payload, pickup_address, delivery_address,
        dimensions: {
          width, length, height,
        },
      } = req.body;    
    await putLoadById(id, userId, name, payload, pickup_address, delivery_address, {width, length, height});
  
    res.json({message: 'Load updated successfully'});
}));

router.post('/:id/post', shipperRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    let load = await getLoadById(id);
    if (!load) {
        throw new InvalidRequestError('No Load with such id found!');
    }
    console.log(load.payload, load.dimensions.width, load.dimensions.length, load.dimensions.height)

    let truck = await findTruckToLoad(load.payload, load.dimensions.width, load.dimensions.length, load.dimensions.height)
    if (!truck) {
        throw new InvalidRequestError('No suatible truck found!')
    }
    load = updateLoadById(id, 'ASSIGNED', truck.assigned_to, 'En route to Pick Up')

    res.json({
      message: 'Load posted successfully',
      driver_found: true,
    });
}));

  

  
module.exports = {
    loadsRouter: router
}