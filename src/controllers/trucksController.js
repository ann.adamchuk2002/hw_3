const express = require('express');
const router = express.Router();
const {driverRole} = require('../middlewares/userMiddleware'); 

const {
    getTrucksByUserId,
    addTruckToUser,
    getTruckById,
    deleteTruckById,
    assignTruckToUser,
    putTruckById
} = require('../services/truckService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

const {
    InvalidRequestError
} = require('../utils/errors');

router.get('/', driverRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    console.log("AFDS");
    const trucks = await getTrucksByUserId(userId);
    res.json({ 
        "trucks": trucks
    })
}));

router.get('/:id', driverRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    console.log(id);
    const truck = await getTruckById(id, userId)
    if (!truck) {
        throw new InvalidRequestError('No Truck with such id found!');
    }
    res.json({truck});
}));

router.post('/', driverRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    await addTruckToUser(userId, req.body.type);
    res.json({message: "Truck created successfully"});
}));



router.post('/:id/assign', driverRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    trucks = getTrucksByUserId(userId);
    await assignTruckToUser(id, userId)

    res.json({message: "Truck assigned successfully"});
}));

router.delete('/:id', driverRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    await deleteTruckById(id, userId)

    res.json({message: "Truck deleted successfully"});
}));

router.put('/:id', driverRole, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const type = req.body.type;
  
    await putTruckById(id, userId, type);
  
    res.json({message: 'Truck updated successfully'});
  }));

  
module.exports = {
    trucksRouter: router
}