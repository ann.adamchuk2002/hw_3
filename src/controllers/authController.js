const express = require('express');
const router = express.Router();

const {
    register,
    login
} = require('../services/authService');

const {
    asyncWrapper
} = require('../utils/apiUtils');


router.post('/register', asyncWrapper(async (req, res) => {
    const {
        email,
        password,
        role
    } = req.body;
    if (role != "DRIVER" || role != "SHIPPER")
    {
        //Role error
    }
    await register({email, password, role});

    res.json({message: 'Success!'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
    const {
        email,
        password
    } = req.body;
    
    const token = await login({email, password});

    res.status(200).json({
        message: 'Success!',
        jwt_token: token
    });
}));

module.exports = {
    authRouter: router
}