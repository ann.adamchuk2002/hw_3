const express = require('express');
const bcrypt = require('bcrypt');
const router = express.Router();

const {
  User
} = require('../models/userModel');
const {
  getUserInfo, 
  deleteUserProfile,
  changeUsersPassword
} = require('../services/usersService');

const {
  asyncWrapper
} = require('../utils/apiUtils');

router.get('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const info = await getUserInfo(userId);
  res.json({user: {
    '_id': info._id,
    'username': info.username,
    createdDate: info.createdDate
  }});
}));

router.delete('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteUserProfile(userId);
  res.json({'message': 'Success'});
}));

router.patch('/me/password', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const { 
    oldPassword, 
    newPassword } = req.body;
  const user = await User.findOne({_id: userId});
  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Invalid password');
  }
  newPassword = await bcrypt.hash(newPassword, 10);
  
  await changeUsersPassword(userId, newPassword);
  return res.json({message: 'Success'});
}));


module.exports = {
  usersRouter: router
}